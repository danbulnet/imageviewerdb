#include <QDebug>
#include <QMessageBox>
#include <QScrollArea>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::init()
{
    dirsModel.setRootPath("");
    dirsModel.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    ui->twDirectorySelector->setModel(&dirsModel);

    ui->lwFilesSelector->setModel(&filesModel);

    ui->twDirectorySelector->hideColumn(1);
    ui->twDirectorySelector->hideColumn(2);
    ui->twDirectorySelector->hideColumn(3);

    openedFilesModel.setStringList(openedFiles.keys());
    ui->lwOpenedFiles->setModel(&openedFilesModel);

    setMouseTracking(true);
}

void MainWindow::on_twDirectorySelector_clicked(const QModelIndex &index)
{
    currentDirectory = dirsModel.filePath(index);
    QDir recoredDir(currentDirectory);
    QStringList allFiles { recoredDir.entryList(QDir::Files) };
    QStringList filteredFiles;
    for (auto& file : allFiles)
    {
        if (file.endsWith(".jpg") ||
            file.endsWith(".jpeg") ||
            file.endsWith(".png"))
        {
            filteredFiles.append(file);
        }
    }
    ui->lwFilesSelector->clearSelection();
    filesModel.setStringList(filteredFiles);
}

void MainWindow::on_pbAddFile_clicked()
{
    for (auto& file : ui->lwFilesSelector->selectionModel()->selectedIndexes())
    {
        QString filename{ currentDirectory + "/" + file.data().toString() };
        if (!openedFiles.contains(filename))
        {
            QFile* newFile{ new QFile(filename) };
            if (!newFile->open(QIODevice::ReadWrite))
            {
                qDebug() << "Can't open file" << filename;
                return;
            }

            openedFiles[filename] = newFile;
        }
    }
    openedFilesModel.setStringList(openedFiles.keys());
}

void MainWindow::on_pbCloseFile_clicked()
{
    openedFiles[currentFile]->close();
    openedFiles.remove(currentFile);
    currentFile = "";
    openedFilesModel.setStringList(openedFiles.keys());
    pixmap.detach();
    ui->lImage->setText("No file selected");
    ui->actionZoom_In->setEnabled(false);
    ui->actionZoom_Out->setEnabled(false);
    ui->actionRotate_Left->setEnabled(false);
    ui->actionRotate_Right->setEnabled(false);
}

void MainWindow::on_lwOpenedFiles_clicked(const QModelIndex &index)
{
    currentFile = index.data().toString();
    pixmap.load(currentFile);
    imgWidth = ui->lImage->size().width();
    imgHeight = ui->lImage->size().height();
    ui->lImage->setPixmap(
                pixmap.scaled(imgWidth,
                              imgHeight,
                              Qt::KeepAspectRatio,
                              Qt::FastTransformation));
    ui->actionZoom_In->setEnabled(true);
    ui->actionZoom_Out->setEnabled(true);
    ui->actionRotate_Left->setEnabled(true);
    ui->actionRotate_Right->setEnabled(true);
}

void MainWindow::on_splitter_2_splitterMoved(int, int)
{
    imgWidth = ui->lImage->size().width();
    imgHeight = ui->lImage->size().height();
    ui->lImage->setPixmap(
                pixmap.scaled(imgWidth,
                              imgHeight,
                              Qt::KeepAspectRatio,
                              Qt::FastTransformation));
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);
   imgWidth = ui->lImage->size().width();
   imgHeight = ui->lImage->size().height();
   ui->lImage->setPixmap(
               pixmap.scaled(imgWidth,
                             imgHeight,
                             Qt::KeepAspectRatio,
                             Qt::FastTransformation));
}

void MainWindow::mouseMoveEvent(QMouseEvent *_event)
{
    QString message{ "x: " +  QString::number(_event->localPos().x()) +
                   ", y: " + QString::number(_event->localPos().y()) };
    ui->statusBar->showMessage(message);
}

void MainWindow::on_actionFull_Screen_Mode_toggled(bool arg1)
{
    if (arg1)
    {
        showFullScreen();
    }
    else
    {
        showNormal();
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Autorem tego programu jest Daniel Bulanda");
    msgBox.exec();
}

void MainWindow::on_actionZoom_In_triggered()
{
    imgWidth = imgWidth * 1.2;
    imgHeight = imgHeight * 1.2;
    ui->lImage->setPixmap(
                pixmap.scaled(imgWidth,
                              imgHeight,
                              Qt::KeepAspectRatio,
                              Qt::FastTransformation));
}

void MainWindow::on_actionZoom_Out_triggered()
{
    imgWidth = imgWidth * 0.8;
    imgHeight = imgHeight * 0.8;
    ui->lImage->setPixmap(
                pixmap.scaled(imgWidth,
                              imgHeight,
                              Qt::KeepAspectRatio,
                              Qt::FastTransformation));
}

void MainWindow::on_actionRotate_Left_triggered()
{
    QMatrix rm;
    rm.rotate(-90);
    pixmap = pixmap.transformed(rm);
    ui->lImage->setPixmap(
                pixmap.scaled(imgWidth,
                              imgHeight,
                              Qt::KeepAspectRatio,
                              Qt::FastTransformation));
}

void MainWindow::on_actionRotate_Right_triggered()
{
    QMatrix rm;
    rm.rotate(90);
    pixmap = pixmap.transformed(rm);
    ui->lImage->setPixmap(
                pixmap.scaled(imgWidth,
                              imgHeight,
                              Qt::KeepAspectRatio,
                              Qt::FastTransformation));
}
