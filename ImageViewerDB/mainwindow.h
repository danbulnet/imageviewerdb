#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QStringListModel>
#include <QMouseEvent>
#include <QPicture>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void resizeEvent(QResizeEvent* event);
    void mouseMoveEvent(QMouseEvent *_event);

private slots:
    void on_twDirectorySelector_clicked(const QModelIndex &index);

    void on_pbAddFile_clicked();

    void on_pbCloseFile_clicked();

    void on_lwOpenedFiles_clicked(const QModelIndex &index);

    void on_splitter_2_splitterMoved(int pos, int index);

    void on_actionFull_Screen_Mode_toggled(bool arg1);

    void on_actionAbout_triggered();

    void on_actionZoom_In_triggered();

    void on_actionZoom_Out_triggered();

    void on_actionRotate_Left_triggered();

    void on_actionRotate_Right_triggered();

private:
    void init();

    QMap<QString, QFile*> openedFiles;
    QString currentDirectory{ "" };
    QString currentFile{ "" };
    QFileSystemModel dirsModel;
    QStringListModel filesModel;
    QStringListModel openedFilesModel;
    QPicture picture;
    QPixmap pixmap;
    int imgWidth{ 0 };
    int imgHeight{ 0 };
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
